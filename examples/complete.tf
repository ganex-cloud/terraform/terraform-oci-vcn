module "vcn-infra" {
  source          = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-vcn.git?ref=master"
  compartment_id  = module.iam_compartment-infra.compartment_id
  cidr_block      = "10.0.0.0/16"
  dns_label       = "infra"
  display_name    = "infra"
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.0.0/24"]
}
