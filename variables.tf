variable "create_vcn" {
  description = "(Required) Controls if VCN should be created (it affects almost all resources)"
  type        = bool
  default     = true
}

variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the compartment to contain the VCN."
  type        = string
  default     = ""
}

variable "cidr_block" {
  description = "(Required) The CIDR IP address block of the VCN."
  default     = "10.0.0.0/16"
  type        = string
}

variable "dns_label" {
  description = "(Optional) A DNS label for the VCN, used in conjunction with the VNIC's hostname and subnet's DNS label to form a fully qualified domain name (FQDN) for each VNIC within this subnet."
  type        = string
  default     = null
}

variable "dns_label_public_subnet" {
  description = "(Optional) (Optional) A DNS label for the public subnet, used in conjunction with the VNIC's hostname and VCN's DNS label to form a fully qualified domain name (FQDN) for each VNIC within this subnet."
  type        = string
  default     = "public"
}

variable "dns_label_private_subnet" {
  description = "(Optional) (Optional) A DNS label for the rrivate subnet, used in conjunction with the VNIC's hostname and VCN's DNS label to form a fully qualified domain name (FQDN) for each VNIC within this subnet."
  type        = string
  default     = "private"
}

variable "display_name" {
  description = "(Optional) (Updatable) A user-friendly name. Does not have to be unique, and it's changeable."
  type        = string
  default     = null
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = {}
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = {}
}

variable "ipv6cidr_blocks" {
  # https://www.terraform.io/docs/providers/oci/r/core_vcn.html#ipv6cidr_block
  description = "(Optional) If you enable IPv6 for the VCN (see isIpv6Enabled), you may optionally provide an IPv6 /48 CIDR block from the supported ranges"
  type        = list(string)
  default     = []
}

variable "is_ipv6enabled" {
  description = "(Optional) IPv6 is currently supported only in the Government Cloud. Whether IPv6 is enabled for the VCN."
  type        = bool
  default     = false
}

variable "enable_service_gateway" {
  # https://www.terraform.io/docs/providers/oci/r/core_service_gateway.html
  description = "(Optional) Creates a new service gateway in the specified compartment."
  type        = bool
  default     = true
}

variable "public_subnets" {
  description = "(Optional) A list of public subnets inside the VCN"
  type        = list(string)
  default     = []
}

variable "private_subnets" {
  description = "(Optional) A list of private subnets inside the VCN"
  type        = list(string)
  default     = []
}

variable "public_route_table_additional_rules" {
  # https://www.terraform.io/docs/providers/oci/r/core_route_table.html#route_rules-1
  description = "(Optional) (Updatable) The collection of public rules used for routing destination IPs to network devices."
  type        = list(map(string))
  default     = []
}

variable "private_route_table_additional_rules" {
  # https://www.terraform.io/docs/providers/oci/r/core_route_table.html#route_rules-1
  description = "(Optional) (Updatable) The collection of private rules used for routing destination IPs to network devices."
  type        = list(map(string))
  default     = []
}

variable "nat_gateway_block_traffic" {
  # https://www.terraform.io/docs/providers/oci/r/core_nat_gateway.html#block_traffic
  description = "(Optional) (Updatable) Whether the NAT gateway blocks traffic through it."
  type        = bool
  default     = false
}
