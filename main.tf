resource "oci_core_vcn" "this" {
  count          = var.create_vcn ? 1 : 0
  cidr_block     = var.cidr_block
  compartment_id = var.compartment_id
  display_name   = var.display_name
  dns_label      = var.dns_label
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags
  #ipv6cidr_blocks = var.ipv6cidr_blocks
  #is_ipv6enabled  = var.is_ipv6enabled
}

resource "oci_core_internet_gateway" "this" {
  count          = length(var.public_subnets) > 0 ? 1 : 0
  compartment_id = var.compartment_id
  display_name   = "Internet Gateway"
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags
  vcn_id         = oci_core_vcn.this[0].id
}

resource "oci_core_nat_gateway" "this" {
  count          = length(var.private_subnets) > 0 ? 1 : 0
  compartment_id = var.compartment_id
  display_name   = "Nat Gateway"
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags
  block_traffic  = var.nat_gateway_block_traffic
  vcn_id         = oci_core_vcn.this[0].id
}

resource "oci_core_route_table" "public" {
  count          = length(var.public_subnets) > 0 ? 1 : 0
  compartment_id = var.compartment_id
  display_name   = "public"
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags

  route_rules {
    destination       = "0.0.0.0/0"
    network_entity_id = oci_core_internet_gateway.this[0].id
  }

  dynamic "route_rules" {
    for_each = var.public_route_table_additional_rules
    content {
      destination       = lookup(route_rules.value, "destination")
      destination_type  = lookup(route_rules.value, "destination_type")
      network_entity_id = lookup(route_rules.value, "network_entity_id")
    }
  }

  vcn_id = oci_core_vcn.this[0].id
}

resource "oci_core_route_table" "private" {
  count          = length(var.private_subnets) > 0 ? 1 : 0
  compartment_id = var.compartment_id
  display_name   = "private"
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags

  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_nat_gateway.this[0].id
  }

  dynamic "route_rules" {
    for_each = var.enable_service_gateway == true ? list(1) : []
    content {
      destination       = lookup(data.oci_core_services.this[0].services[0], "cidr_block")
      destination_type  = "SERVICE_CIDR_BLOCK"
      network_entity_id = oci_core_service_gateway.this[0].id
    }
  }

  dynamic "route_rules" {
    for_each = var.private_route_table_additional_rules
    content {
      destination       = lookup(route_rules.value, "destination")
      destination_type  = lookup(route_rules.value, "destination_type")
      network_entity_id = lookup(route_rules.value, "network_entity_id")
    }
  }

  vcn_id = oci_core_vcn.this[0].id
}

resource "oci_core_subnet" "private" {
  count                      = var.create_vcn && length(var.private_subnets) > 0 ? length(var.private_subnets) : 0
  cidr_block                 = var.private_subnets[count.index]
  compartment_id             = var.compartment_id
  display_name               = "private-${count.index}"
  dns_label                  = "${var.dns_label_private_subnet}${count.index}"
  prohibit_public_ip_on_vnic = true
  route_table_id             = oci_core_route_table.private[0].id
  vcn_id                     = oci_core_vcn.this[0].id
  defined_tags               = var.defined_tags
  freeform_tags              = var.freeform_tags
}

resource "oci_core_subnet" "public" {
  count                      = var.create_vcn && length(var.public_subnets) > 0 ? length(var.public_subnets) : 0
  cidr_block                 = var.public_subnets[count.index]
  compartment_id             = var.compartment_id
  display_name               = "public-${count.index}"
  dns_label                  = "${var.dns_label_public_subnet}${count.index}"
  prohibit_public_ip_on_vnic = false
  route_table_id             = oci_core_route_table.public[0].id
  vcn_id                     = oci_core_vcn.this[0].id
  defined_tags               = var.defined_tags
  freeform_tags              = var.freeform_tags
}

resource "oci_core_service_gateway" "this" {
  count          = var.enable_service_gateway == true ? 1 : 0
  compartment_id = var.compartment_id
  display_name   = "Service Gateway"
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags
  services {
    service_id = lookup(data.oci_core_services.this[0].services[0], "id")
  }
  vcn_id = oci_core_vcn.this[0].id
}

# DHCP Options
#resource "oci_core_dhcp_options" "this" {
#  count          = var.create_vcn && length(var.dhcp_options) > 0 ? 1 : 0
#  compartment_id = var.compartment_id
#  display_name   = var.display_name
#  defined_tags   = var.defined_tags
#  freeform_tags  = var.freeform_tags
#  vcn_id         = oci_core_vcn.this[0].id
#
#  dynamic "options" {
#    for_each = lookup(var.dhcp_options, "type") == "DomainNameServer" ? [lookup(var.dhcp_options, "type", {})] : []
#    content {
#      type               = "DomainNameServer"
#      server_type        = "CustomDnsServer"
#      custom_dns_servers = lookup(var.dhcp_options, "custom_dns_servers")
#    }
#  }
#
#  dynamic "options" {
#    for_each = lookup(var.dhcp_options, "type") == "SearchDomain" ? [lookup(var.dhcp_options, "type", {})] : []
#    content {
#      type        = "DomainNameServer"
#      server_type = "VcnLocalPlusInternet"
#    }
#  }
#
#  dynamic "options" {
#    for_each = length(lookup(var.dhcp_options, "search_domain_names", [])) == 0 ? [] : [lookup(var.dhcp_options, "search_domain_names")]
#    content {
#      type                = "SearchDomain"
#      search_domain_names = lookup(var.dhcp_options, "search_domain_names")
#    }
#  }
#}
#
#
