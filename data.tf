data "oci_identity_availability_domains" "availability_domains_list" {
  compartment_id = var.compartment_id
}

data "template_file" "availability_domains_names" {
  count    = length(data.oci_identity_availability_domains.availability_domains_list.availability_domains)
  template = lookup(data.oci_identity_availability_domains.availability_domains_list.availability_domains[count.index], "name")
}

data "oci_core_vcn" "this" {
  vcn_id = oci_core_vcn.this[0].id
}

data "oci_core_services" "this" {
  count = var.enable_service_gateway == true ? 1 : 0
  filter {
    name   = "name"
    values = ["All .* Services In Oracle Services Network"]
    regex  = true
  }
}
