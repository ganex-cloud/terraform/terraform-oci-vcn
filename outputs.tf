output "vcn_id" {
  description = "id of vcn that is created"
  value       = oci_core_vcn.this[0].id
}

output "vcn_cidr_block" {
  description = "The CIDR IP address block of the VCN. "
  value       = oci_core_vcn.this[0].cidr_block
}

output "vcn_nat_gateway_id" {
  description = "id of nat gateway if it is created"
  value       = join(",", oci_core_nat_gateway.this.*.id)
}

output "vcn_internet_gateway_id" {
  description = "id of nat gateway if it is created"
  value       = join(",", oci_core_internet_gateway.this.*.id)
}

output "vcn_service_gateway_id" {
  description = "id of nat gateway if it is created"
  value       = join(",", oci_core_service_gateway.this.*.id)
}

output "vcn_public_subnets_id" {
  description = "List of IDs of public subnets"
  value       = oci_core_subnet.public.*.id
}

output "vcn_private_subnets_id" {
  description = "List of IDs of private subnets"
  value       = oci_core_subnet.private.*.id
}

output "vcn_public_subnets_cidr_block" {
  description = "The CIDR IP address block of the VCN. "
  value       = oci_core_subnet.public.*.cidr_block
}

output "vcn_private_subnets_cidr_block" {
  description = "The CIDR IP address block of the VCN. "
  value       = oci_core_subnet.private.*.cidr_block
}

output "vcn_availability_domains" {
  description = "list of ADs in the selected region"
  value       = sort(data.template_file.availability_domains_names.*.rendered)
}

output "vcn_private_route_id" {
  description = "id of internet gateway route table"
  value       = join(",", oci_core_route_table.private.*.id)
}

output "vcn_public_route_id" {
  description = "id of VCN NAT gateway route table"
  value       = join(",", oci_core_route_table.public.*.id)
}
